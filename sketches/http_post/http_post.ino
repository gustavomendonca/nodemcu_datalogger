#include <ESP8266HTTPClient.h>
#include <ESP8266WiFi.h>

#define SSID "dell"
#define SENHA_WIFI "20703JHN"
#define ENDPOINT_REGISTRO "http://10.42.0.1:4000/datalogger/registro"
void setup() {
 
  Serial.begin(9600);
  WiFi.begin(SSID, SENHA_WIFI);
 
  while (WiFi.status() != WL_CONNECTED) {  //Wait for the WiFI connection completion
    delay(500);
    Serial.println("Aguardando conexão com rede...");
  }
}
 
void loop() {
 
 if(WiFi.status()== WL_CONNECTED){   //Check WiFi connection status
 
   HTTPClient http;    //Declare object of class HTTPClient
 
   http.begin(ENDPOINT_REGISTRO);      //Specify request destination
   http.addHeader("Content-Type", "application/json");  //Specify content-type header
 
   int httpCode = http.POST("{\"id\":12344}");   //Send the request
   String payload = http.getString();                  //Get the response payload
 
   Serial.println(httpCode);   //Print HTTP return code
   Serial.println(payload);    //Print request response payload
 
   http.end();  //Close connection
 
 }else{
 
    Serial.println("Error in WiFi connection");   
 
 }
 
  delay(3000);  //Send a request every 30 seconds
 
}
