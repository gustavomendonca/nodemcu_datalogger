// Inluir a biblioteca EmonLib, disponivel no repositório https://github.com/openenergymonitor/EmonLib

#include "EmonLib.h"

// Instancia do monitor de energia, classe disponibilizada na biblioteca EmonLib, que disponibiliza
EnergyMonitor Amperimetro;

// Variável para ler a corrente
double I;

// Definição do pino de leitura do amperimetro
int pinAmp = A0;

void setup() {

  // Inicializa a porta serial
  Serial.begin(9600);

  // Inicializa o monitor de  energia como um medidor de corrente Amperimetro.current(<pino>,<valor de calibração>);
  Amperimetro.current(pinAmp, 111.1);

  // O programa de exemplo da biblioteca usa o valor de calibração igual a 111.1, porém outros exemplos disponiveis na web usam valores como o da linha abaixo
  //Amperimetro.current(pinAmp, 6.0606);
  // Referencia para obtenção dos valores de calibração https://portal.vidadesilicio.com.br/sct-013-sensor-de-corrente-alternada/

}

void loop() {

  // Obtêm o valor de corrente
  // o numero passado como parâmetro é o numero de amostras obtidas do sensor.
  I = Amperimetro.calcIrms(1480);

  // Imprime o valor de corrente na porta serial
  Serial.println(I);

}
