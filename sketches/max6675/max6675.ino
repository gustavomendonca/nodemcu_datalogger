#include <max6675.h>
#include <Wire.h>

int ktcSO = 12;
int ktcCS = 13;
int ktcCLK = 14;

MAX6675 ktc(ktcCLK, ktcCS, ktcSO);
  
void setup() {
  Serial.begin(9600);
}

void loop() {

  float DC = ktc.readCelsius();
  Serial.print("C = "); 
  Serial.println(ktc.readCelsius());

  delay(1000);
}

