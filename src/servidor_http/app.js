
(function () {
    const express = require('express')
    const app = express()
    const bodyParser = require('body-parser')
    const rotas = require('./rotas')
  
    const apiPort = process.env.PORTA_SERVIDOR || 4000
  
    app.use(bodyParser.json())
  
    app.use((req, res, next) => {
      let now = new Date
      console.log(`[${now.toISOString().replace('T', ' ').slice(0, 23)}] ${req.method} em ${req.path} body: ${req.body ? JSON.stringify(req.body) : 'vazio'}`)
      next()
    })
  
    app.use('/datalogger', rotas)
  
    // inicia API de controle  
    app.listen(apiPort, function () {
      console.log('Servidor iniciado na porta', apiPort)
    })
  
  })()