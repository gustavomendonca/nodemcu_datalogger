#include <Wire.h>
#include <RtcDS1307.h>
#include <max6675.h>
#include <PubSubClient.h>
#include <EmonLib.h>
#include <ESP8266WiFi.h>


// SPI
#define SPI_CLK 14
#define SPI_MISO 12
#define SPI_MOSI ??
#define TERMOMETRO_1_CS 13

// WIFI
#define SSID "TCS_VIVO"
#define SENHA_WIFI "33331999"

// MQTT
#define MQTT_SERVER "192.168..168"
#define MQTT_PORT 1883
#define MASCARA_PAYLOAD "{\"data\": %d,\"valor\": %.2f}"

//AMPERIMETRO
#define PORTA_AMPERIMETRO A0
#define VALOR_CALIBRACAO 20.5

// sensores
#define TOPICO_SENSOR_TEMP_1 "node1/temperatura1"
#define TOPICO_SENSOR_CORRENTE_1 "node1/corrente1"

#define SEND_TOPIC_PIN D0
#define WIFI_STATUS_PIN D4


#define DEBUG 1

RtcDS1307<TwoWire> Rtc(Wire);
MAX6675 termometro_1(SPI_CLK, TERMOMETRO_1_CS, SPI_MISO);
WiFiClient espClient;
PubSubClient client(espClient);
EnergyMonitor amperimetro;

char buf[50];

void setup () {
  #if DEBUG
  Serial.begin(115200);
  #endif
  setupRTC();
  setupAmperimetro();
  setupMQTT();
  setPinStatus();
}

void loop () {
  if (!client.connected()) {
    reconectar();
  }
  else wifiStatus(2);
  client.loop();
  uint32_t agora = obterDataAtual();
  #if DEBUG
  imprimeData(agora);
  #endif
  float corr1 = obterCorrente(amperimetro);
  float temp1 = obterTemperatura(termometro_1);
  publicarRegistro(TOPICO_SENSOR_TEMP_1, agora, temp1);
  publicarRegistro(TOPICO_SENSOR_CORRENTE_1, agora, corr1);
  delay(1000);
}

void setPinStatus(){
  pinMode(SEND_TOPIC_PIN, OUTPUT);
  digitalWrite(SEND_TOPIC_PIN, HIGH);
  pinMode(WIFI_STATUS_PIN, OUTPUT);
  digitalWrite(WIFI_STATUS_PIN, HIGH);
}

void wifiStatus(int status){
  switch(status){
    case 0:
      digitalWrite(WIFI_STATUS_PIN, HIGH);
      break;
    case 1:
      digitalWrite(WIFI_STATUS_PIN, LOW);
      break;
    case 2:
      digitalWrite(WIFI_STATUS_PIN, HIGH);
      delay(25);
      digitalWrite(WIFI_STATUS_PIN, LOW);
      delay(25);
      break;
  }    
}

void topicStatus(int status){
  switch(status){
    case 0:
      digitalWrite(SEND_TOPIC_PIN, LOW);
      delay(50);
      digitalWrite(SEND_TOPIC_PIN, HIGH);
      delay(50);
      digitalWrite(SEND_TOPIC_PIN, LOW);
      delay(50);
      digitalWrite(SEND_TOPIC_PIN, HIGH);
      break;
    case 1:
      digitalWrite(SEND_TOPIC_PIN, LOW);
      delay(25);
      digitalWrite(SEND_TOPIC_PIN, HIGH);
      break;
  }  
}

void setupMQTT() {
  client.setServer(MQTT_SERVER, MQTT_PORT);
  client.setCallback(callbackMensagemRecebida);
}

void setupAmperimetro() {
  amperimetro.current(PORTA_AMPERIMETRO, VALOR_CALIBRACAO);
}

float obterCorrente(EnergyMonitor& amperimetro) {
  float aux = 0;
  int qtdLeituras = 10;
  
  for (int i = 0; i < qtdLeituras; i++)
  {
    //Calcula a corrente
    aux += amperimetro.calcIrms(1480);
    delay(10);
  }
  aux /= qtdLeituras;
  #if DEBUG
  Serial.print("Leitura do sensor de corrente: ");
  Serial.println(aux);
  #endif
  return aux;
}

void setupRTC() {
  Wire.begin();
  Rtc.Begin();

  // no inicio da execução, verifica se RTC está com data correta, senão o inicia com data da compilação da sketch
  if (!Rtc.IsDateTimeValid()) {
    #if DEBUG
    Serial.println("Data na RTC inválida, Iniciando com data atual...");
    #endif
    RtcDateTime agora = RtcDateTime(__DATE__, __TIME__);
    Rtc.SetDateTime(agora);
  }

  if (!Rtc.GetIsRunning()) {
    #if DEBUG
    Serial.println("RTC desativado, ativando...");
    #endif
    Rtc.SetIsRunning(true);
  }
  Rtc.SetSquareWavePin(DS1307SquareWaveOut_Low);
}

uint32_t obterDataAtual() {

  if (!Rtc.IsDateTimeValid()) {
    // TODO: verificar se é possível recuperar um estado válido de data
    #if DEBUG
    Serial.println("RTC não possui uma data válida");
    #endif
  }
  RtcDateTime dataAtual = Rtc.GetDateTime();
  return dataAtual.Epoch32Time();
}

//retorna código HTTP
int publicarRegistro(char* topico, int data, float valor) {
  int enviado = 0;
  if (WiFi.status() == WL_CONNECTED) {

    sprintf(buf, MASCARA_PAYLOAD, data, valor);
    Serial.print("buf: ");
    Serial.println(topico);
    enviado = client.publish(topico, buf);

    if (enviado) {
      #if DEBUG
      Serial.println("registro publicado com sucesso");
      #endif
      topicStatus(1);
    }
    else {
      #if DEBUG
      Serial.println("erro na publicação do registro");
      #endif
      topicStatus(0);
    }
  }
  else {
    #if DEBUG
    Serial.println("Envio não possivel, sem acesso à rede");
    #endif
  }
  return enviado;
}

void callbackMensagemRecebida(char* topic, byte* payload, unsigned int length) {
  #if DEBUG
  Serial.print("Messagem recebida [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  Serial.println();
  #endif
}

float obterTemperatura(MAX6675& termometro) {
  float temperatura = termometro.readCelsius();
  #if DEBUG
  Serial.print("Leitura do sensor de temperatura: ");
  Serial.println(temperatura);
  #endif
  return temperatura;
}

void reconectar() {
  while (!client.connected()) {
    if (WiFi.status() != WL_CONNECTED) {
      wifiStatus(0);
      Serial.println("Conectando em rede wireless...");
      // O erro é devido a conexão wifi
      WiFi.begin(SSID, SENHA_WIFI);
      delay(5000);
    } 
    else {
      Serial.println("Attempting MQTT connection...");
      // Se o erro não é devido a falha no WIFI:
      if (client.connect("ESP8266Client")) {
        #if DEBUG
        Serial.println("connected");
        #endif
      } 
      else {
        wifiStatus(1);
        #if DEBUG
        Serial.print("failed, rc=");
        Serial.print(client.state());
        Serial.println(" try again in 5 seconds");
        #endif
        delay(5000);
      }
    }
  }
}

#if DEBUG
void imprimeData(const RtcDateTime& dt) {
  Serial.print(dt.Day());
  Serial.print("/");
  Serial.print(dt.Month());
  Serial.print("/");
  Serial.print(dt.Year());
  Serial.print(" ");
  Serial.print(dt.Hour());
  Serial.print(":");
  Serial.print(dt.Minute());
  Serial.print(":");
  Serial.print(dt.Second());
  Serial.println();
}
#endif
