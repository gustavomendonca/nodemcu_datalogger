const mosca = require('mosca')
const MongoClient = require('mongodb').MongoClient
const ascoltatori = require('ascoltatori')
const express = require('express')
const app = express()
const server = require('http').Server(app);
const socketio = require('socket.io')(server)
const porta = process.env.PORT || 3000

app.set('port', porta)
app.set('views', __dirname + '/views')
app.set('view engine', 'jade')
app.use(express.static(__dirname + '/public'))

app.get('/', (req, res) => {
  res.render('index', { title: 'Datalogger NodeMCU - Montagem Final' })
})

// URL de conexão do mongo
let mongo_host = process.env.MONGODB_HOST || 'mongodb://localhost:27017'  
// Nome do DB
let mongo_db = process.env.MONGODB_DB || 'mqtt'

let db = null

// conecta com mongoDB
MongoClient.connect(mongo_host + '/' + mongo_db, {}, (err, dbMongo) => {
  if (err) console.log(`Erro na conexão com mongoDB: ${err}`)
  else {
    db = dbMongo
    console.log('Conexão com MongoDB bem sucedida')
    start()
  }
})
    
const start = () => {  
  
  const ascoltatoriSettings = {
    type: 'mongo',
    db: db,
    pubsubCollection: 'ascoltatori',
    mongo: {}
  }

  const moscaSettings = {
    id: 'mymosca', // used to publish in the $SYS/<id> topicspace
    port: 1883,
    stats: false, // publish stats in the $SYS/<id> topicspace
    // logger: {
    //   level: 'debug'
    // },
    backend: {
      type: 'mongodb',
      url: mongo_host + '/' + mongo_db
    },
    persistence: {
      factory: mosca.persistence.Mongo,
      url: mongo_host + '/' + mongo_db
    },
    backend: ascoltatoriSettings
  }

  const serverMQTT = new mosca.Server(moscaSettings);

  serverMQTT.on('ready', () => {
    console.log('Servidor MQTT iniciado na porta 1883')
  })

  serverMQTT.on('clientConnected', (client) => {
    console.log('cliente conectado:', client.id);		
  })

  serverMQTT.on('published', (pacote, cliente) => {
    //console.log('Published', pacote.payload);
  })

  const registros = db.collection('registros')

  socketio.sockets.on('connection', (socket) => {
    ascoltatori.build(ascoltatoriSettings, (err, ascoltatore) => {
      if (err) console.log(`Erro na inicialização do ascoltatore: ${err}`)
      
      // inscreve nos tópicos
      ascoltatore.subscribe('node/+/+/*', (topico, mensagem) => {
        
        console.log(`topico: ${topico}, mensagem: ${JSON.stringify(mensagem)}`)
        
        let caminho = topico.split('/') // cria array com elementos do topico separados pelas barras '/'
        
        let registro = {
          nodeId: caminho[1],
          sensor: caminho[2],
          data: new Date(mensagem.data * 1000), // cria data a partir do valor de data no formato EPOCH recebido
          valor: mensagem.valor
        }
        
        registros.insertOne(registro, (err, res) => {
          if (err) console.log(`Erro ao gravar mensagem no mongoDB: ${err}`)
        })
        
      })

      // republica temperatura formatada
      ascoltatore.subscribe('node/+/temperatura/*', (topico, mensagem) => {
        ascoltatore.publish('valores/temperatura', mensagem.valor, () => {})
        socket.emit('leitura_temperatura', topico, mensagem)
      })

      // republica corrente formatada
      ascoltatore.subscribe('node/+/corrente/*', (topico, mensagem) => {
        ascoltatore.publish('valores/corrente', mensagem.valor, () => {})
        socket.emit('leitura_corrente', topico, mensagem)
      })
    })
  })

  socketio.on('connection', function(client) {  
    console.log('Socket adicionado...');
  })

  // inicia servidor
  server.listen(porta, () => {
    console.log('Servidor web iniciado na porta', porta)
  })

}
