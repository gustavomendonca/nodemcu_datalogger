var ChartTemperatura;
var optionChartTemperatura = {
  tooltip : { 
    formatter: "{a} <br/>{c}°C"
  },
  series: [
    {
      name: 'Temperatura',
      type: 'gauge',
      detail: {formatter:'{value} °C'},
      data: [{value: 0}]
    }
    ]
  };

var ChartCorrente;
var optionChartCorrente = {
  tooltip : { 
    formatter: "{a} <br/>{c} A"
  },
  series: [
    {
      name: 'Corrente',
      type: 'gauge',
      min: 0,
      max: 20,
      detail: {formatter:'{value} A'},
      data: [{value: 0}]
    }
  ]
};

var historicoTemperatura = [];
var ChartTendenciaTemperatura;
var optionChartTendenciaTemperatura = {
  title: {
      text: 'Histórico de temperatura'
  },
  tooltip: {
      trigger: 'axis',
      formatter: function (params) {
          params = params[0];
          var date = new Date(params*1000);
          return date;
      },
      axisPointer: {
        animation: false
      }
  },
  xAxis: {
      type: 'time',
      splitLine: {
          show: false
      }
  },
  yAxis: {
      type: 'value',
      boundaryGap: [0, 100],
      splitLine: {
          show: false
      }
  },
  series: [{
      name: 'Histórico de temperatura',
      type: 'line',
      showSymbol: false,
      hoverAnimation: false,
      data: historicoTemperatura
  }]
};

var template = _.template(
    '<span class="date"><%= new Date(message.data * 1000) %></span>' +
    '<span class="topic"><%= topic %></span>' +
    '<span class="message"><%= message.valor %></span>' 
  );
  
var setupSockets = function () {
  var messages = document.querySelector('#message-list');

  var socket = window.socket = io.connect();
  socket.on('connect', function () {
    var status = document.querySelector('#status');
    status.className = 'badge badge-success';
    status.innerText = 'Online';
  });

  socket.on('leitura_temperatura', (topic, message) => {
    optionChartTemperatura.series[0].data[0].value = message.valor;
    ChartTemperatura.setOption(optionChartTemperatura, true);

    historicoTemperatura.push([new Date(message.data * 1000), message.valor]);
    optionChartTendenciaTemperatura.series[0].data= historicoTemperatura;
    ChartTendenciaTemperatura.setOption(optionChartTendenciaTemperatura);

    adicionaMensagem(topic, message);
  });

  socket.on('leitura_corrente', (topic, message) => {
    optionChartCorrente.series[0].data[0].value = message.valor;
    ChartCorrente.setOption(optionChartCorrente, true);
    adicionaMensagem(topic, message);
  });

  function adicionaMensagem(topic, message) {
    var m = document.createElement('p');
    m.innerHTML = template({topic: topic, message: message});
    messages.insertBefore(m, messages.firstChild);
  }
};



window.onload = function() {
  setupSockets();

  ChartTemperatura = echarts.init(document.getElementById('temperatura1_gauge')); 
  ChartTemperatura.setOption(optionChartTemperatura);

  ChartCorrente = echarts.init(document.getElementById('corrente1_gauge')); 
  ChartCorrente.setOption(optionChartCorrente);

  ChartTendenciaTemperatura = echarts.init(document.getElementById('temperatura1_tendencia')); 
  ChartTendenciaTemperatura.setOption(optionChartTendenciaTemperatura);

};
